import os
import sys
import json
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.wsgi

import sockjs.tornado
from snippets.models import Snippet
from tornado_ws.wsgi import application as tornado_wsgi

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

class SnippetConnection(sockjs.tornado.SockJSConnection):
    """Chat connection implementation"""
    # Class level variable
    participants = set()

    def on_open(self, info):
        self.participants.add(self)
        data = [{'title': snippet.title, 'id': snippet.id} for snippet in Snippet.objects.all()]
        json_data = {
            'type': 'snippets',
            'snippets': json.dumps(data)
        }
        self.send(json_data)

    def on_message(self, message):
        data = json.loads(message)
        json_data = ''
        if data['type'] == 'fetch':
            snippet = Snippet.objects.filter(id=data['id']).first()
            snippet_data = {
                'id': snippet.id,
                'title': snippet.title,
                'content': snippet.content
            }
            json_data = {
                'type': 'snippet',
                'status': 'ok' if snippet else '404',
                'snippet': json.dumps(snippet_data)
            }
            self.send(json_data)
        if data['type'] == 'create':
            snippet_data = data['snippet']
            snippet = Snippet.objects.create(**snippet_data)
            data = [{'title': snippet.title, 'id': snippet.id} for snippet in Snippet.objects.all()]
            json_data = {
                'type': 'snippets',
                'status': 'created',
                'snippets': json.dumps(data)
            }
            self.broadcast(self.participants, json_data)

    def on_close(self):
        # Remove client from the clients list and broadcast leave message
        self.participants.remove(self)

if __name__ == "__main__":
    SnippetRouter = sockjs.tornado.SockJSRouter(SnippetConnection, '/sockjs')
    app = tornado.web.Application(
        SnippetRouter.url + [(r'/(.*)', tornado.web.FallbackHandler, dict(
        fallback=tornado.wsgi.WSGIContainer(tornado_wsgi)))]
    )
    app.listen(8002)
    print 'SockJS Server started'
    tornado.ioloop.IOLoop.instance().start()
