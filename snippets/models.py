from __future__ import unicode_literals

from django.db import models


class Snippet(models.Model):

    title = models.CharField(max_length=255)
    content = models.TextField()

    def __unicode__(self):
        return self.content[:100]


