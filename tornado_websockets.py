#!/usr/bin/env python

import json

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.wsgi
from tornado_ws.wsgi import application as tornado_wsgi

from django.core import serializers
from snippets.models import Snippet


class TornadoWebSocket(tornado.websocket.WebSocketHandler):

    clients = set()

    def check_origin(self, origin):
        return True

    def open(self):
        TornadoWebSocket.clients.add(self)
        data = [{'title': snippet.title, 'id': snippet.id} for snippet in Snippet.objects.all()]
        json_data = {
            'type': 'snippets',
            'snippets': json.dumps(data)
        }
        self.write_message(json_data)

    def on_message(self, message):
        data = json.loads(message)
        json_data = ''
        if data['type'] == 'fetch':
            snippet = Snippet.objects.filter(id=data['id']).first()
            snippet_data = {
                'id': snippet.id,
                'title': snippet.title,
                'content': snippet.content
            }
            json_data = {
                'type': 'snippet',
                'status': 'ok' if snippet else '404',
                'snippet': json.dumps(snippet_data)
            }
            self.write_message(json_data)
        if data['type'] == 'create':
            snippet_data = data['snippet']
            snippet = Snippet.objects.create(**snippet_data)
            data = [{'title': snippet.title, 'id': snippet.id} for snippet in Snippet.objects.all()]
            json_data = {
                'type': 'snippets',
                'status': 'created',
                'snippets': json.dumps(data)
            }
            TornadoWebSocket.broadcast(json_data)


    def on_close(self):
        if self in TornadoWebSocket.clients:
            TornadoWebSocket.clients.remove(self)

    @classmethod
    def broadcast(cls, message):
        for client in cls.clients:
            client.write_message(message)


application = tornado.web.Application([
    (r'/ws', TornadoWebSocket),
    (r'/(.*)', tornado.web.FallbackHandler, dict(
        fallback=tornado.wsgi.WSGIContainer(tornado_wsgi)
    )),
], debug=True)

if __name__ == '__main__':
    application.listen(8001)
    print 'Server started'
    tornado.ioloop.IOLoop.instance().start()

